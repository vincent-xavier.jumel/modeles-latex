# Commettre les fichiers générés dans le dépôt




1. Créer un jeton d'accès avec le droit en écriture 
![image.png](./image.png)
![image-1.png](./image-1.png)
![image-2.png](./image-2.png)
2. Ajouter une variable de CI protégée dans vos paramètres avec le jeton qui vient d'être créé, avec le nom CI_PUSH_TOKEN
![image-3.png](./image-3.png)
3. Ajouter une variable protégéé CI_USERNAME avec le nom qui doit apparaitre pour les commits.

Remarque : si vous souhaitez utiliser ce mécanisme sur une branche non protégée, il faut déclarer les variables comme non-protégées.

Remarque : dans la tâche de mise à jour, le message de commit doit commencer par `[skip ci]` pour ne pas lancer la construction, ce qui conduirait à une boucle infinie.
